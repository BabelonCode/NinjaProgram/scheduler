﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SchedulerExample;

namespace SchedulerExample.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IJobService myJobService1 = JobManager.Scheduler()
                .ToRunOnceIn(5)
                .Seconds()
                .AtStartTime()
                .BuildJobService<MyJob>();

            IJobService myJobService2 = JobManager.Scheduler()
                .ToRunOnceIn(1)
                .Seconds()
                .AtTheEndOfDay()
                .BuildJobService<MyJob>();

            var cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;

            myJobService1.Start(token);
            myJobService2.Start(token);

            Thread.Sleep(15000);

            cancellationTokenSource.Cancel();
            cancellationTokenSource.Dispose();

            Console.ReadLine();
        }
    }

    public class MyJob : IJob
    {
        public void Execute()
        {
            Console.WriteLine(DateTime.Now.ToString("yyyy MM dd HH:mm:ss"));
        }
    }
}
